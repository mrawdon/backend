<?php

namespace Kanban\Transformer;

use League\Fractal\TransformerAbstract;
use Gitlab\Models\Label;

class LabelTransformer extends TransformerAbstract
{
    public function transform(Label $label) 
    {
        $result = [
            'name' => $label->getName(),
            'color' => $label->getColor(),
        ];

        return $result;
    }
}
