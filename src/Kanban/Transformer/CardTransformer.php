<?php

namespace Kanban\Transformer;

use Gitlab\Models\Issue;
use Gitlab\Models\Milestone;
use League\Fractal\TransformerAbstract;
use Silex\Application;

class CardTransformer extends TransformerAbstract
{

    /**
     * @var Application
     */
    protected $app;

    /**
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    protected $defaultIncludes = [
        'milestone',
    ];

    protected $availableIncludes = [
        'project'
    ];

    protected $htmlCommentPattern = '|<!--\s@KB:(.*?)\s-->|';
    protected $todoPattern = '|[-\*]{1}\s(\[.\])(.*)|';

    public function transform(Issue $card)
    {
        $result = [
            'id'          => $card->getId(),
            'iid'         => $card->getIid(),
            'title'       => $card->getTitle(),
            'project_id'  => $card->getProjectId(),
            'labels'      => $card->getLabels(),
            'description' => $this->propertiesClean($this->todoClear($card->getDescription())),
            'author'      => $card->getAuthor(),
            'assignee'    => $card->getAssignee(),
            'state'       => $card->getState(),
            'todo'        => $this->includeTodo($card->getDescription()),
            'properties'  => $this->includeProperties($card->getDescription()),
        ];

        return $result;
    }

    /**
     * Clear description from todo
     *
     * @param string $desc  issue description
     *
     * @return string
     */
    public function todoClear($desc)
    {
        return trim(preg_replace($this->todoPattern,'', $desc), PHP_EOL);
    }

    /**
     * Clear description from html comments
     *
     * @param string $desc  issue description
     *
     * @return string
     */
    public function propertiesClean($desc)
    {
        return trim(preg_replace($this->htmlCommentPattern, '', $desc), PHP_EOL);
    }

    /**
     * Parse description and gets todo list
     *
     * @param string $description issue description
     *
     * @return array
     */
    public function includeTodo($description)
    {
        $matches = [];
        if (preg_match_all($this->todoPattern, $description, $matches)) {

            $todoList = [];

            foreach ($matches[0] as $k => $item) {
                $todoList[] = [
                    'checked' => $matches[1][$k] == '[x]' ? true : false,
                    'body'  => trim($matches[2][$k])
                ];
            }

            return $todoList;
        }

        return [];
    }

    /**
     * Parse html and get issue properties
     *
     * @param $description  issue description
     *
     * @return array
     */
    public function includeProperties($description)
    {
        $matches = [];
        $properties = [
            'andon'   => 'none',
        ];

        if (preg_match($this->htmlCommentPattern, $description, $matches)) {
            $properties = json_decode($matches[1], true);
        }

        return $properties;
    }

    /**
     * Include Milestone
     *
     * @param Issue $card
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeMilestone(Issue $card)
    {
        $milestone = $card->getMilestone();

        if ($milestone instanceOf Milestone) {
            $milestone = $this->item($milestone, new MilestoneTransformer());
        }

        return $milestone;
    }

    /**
     * @param Issue $issue
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeProject(Issue $issue)
    {
        $response = $this->app['gitlab_api']->executeCommand('GetProject', ['project_id' => $issue->getProjectId()]);
        return $this->item($response, new BoardTransformer());
    }
}