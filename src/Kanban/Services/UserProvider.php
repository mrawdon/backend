<?php

namespace Kanban\Services;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Kanban\Model\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Doctrine\DBAL\Connection;

class UserProvider implements UserProviderInterface
{
    const PROVIDER_GITLAB = 'gitlab';
    const PROVIDER_GITHUB = 'github';
    const PROVIDER_BITBUCKT = 'bitbucket';

    private $encoder;

    /**
     * @var \Redis
     */
    private $redis;

    public function __construct($redis, $encoder)
    {
        $this->encoder = $encoder;
        $this->redis = $redis;
    }

    /**
     * @param string $username
     *
     * @return User
     */
    public function loadUserByUsername($username)
    {
        if (! $this->checkUser($username)) {
            throw new BadRequestHttpException(sprintf('Username "%s" does not exist.', $username));
        }

        $user = $this->redis->hmGet('users:' . strtolower($username), $this->getFields());

        return new User($user);
    }

    /**
     * @param int $provider
     * @param int $providerUserId
     *
     * @return bool|User
     *
     */
    public function loadUserByOauth($provider, $providerUserId)
    {
        if (! $userName = $this->redis->get('credentials:' . implode(':', array($provider, $providerUserId)))) {
            return false;
        }

        if (! $user = $this->loadUserByUsername($userName)) {
            return false;
        }

        return new User($user);
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function updateCredential(User $user)
    {
        $this->redis->hMSet('users:' . $user->getUsername(), $this->toArray($user));

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * @param array $data
     * @param array|null $provider
     *
     * @return User
     */
    public function registerUser($data, $provider = null)
    {
        $user = new User(
            [
                'username' => strtolower($data['_username']),
                'email'    => strtolower($data['_email']),
                'password' => $this->encoder->encodePassword($data['_password'], ''),
                'roles'    => 'ROLE_USER',
            ]
        );

        if ($this->checkUser($user->getUsername())) {
            throw new BadRequestHttpException();
        }

        $this->redis->hMSet('users:' . $user->getUsername(), $this->toArray($user));

        if (null !== $provider) {
            $user = $this->loadUserByUsername($user->getUsername());

            $this->redis->set('credentials:' . implode(':', $provider), $user->getUsername());
        }

        return $user;
    }

    public function getAccessToken(User $user)
    {
        $this->generateToken($user);

        return \JWT::encode(['name' => $user->getUsername()], $user->getToken());
    }

    public function generateToken(User $user)
    {
        $user->setToken(openssl_random_pseudo_bytes(10));
        $this->redis->hmSet('users:' . $user->getUsername(), $this->toArray($user));
    }

    /**
     * return true if user exists or false if ne exists
     *
     * @param $username
     *
     * @return bool
     */
    public function checkUser($username)
    {
        return $this->redis->hExists('users:' . strtolower($username), 'username');
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Kanban\Model\User';
    }

    protected function toArray(User $user)
    {
        return [
            'username'    => $user->getUsername(),
            'email'       => $user->getEmail(),
            'password'    => $user->getPassword(),
            'credentials' => json_encode($user->allCredential()),
            'role'        => json_encode($user->getRoles()),
            'token'       => $user->getToken()
        ];
    }

    protected function getFields()
    {
        return [
            'username',
            'email',
            'password',
            'credentials',
            'role',
            'token',
        ];
    }
}
