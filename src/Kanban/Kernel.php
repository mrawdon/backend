<?php

namespace Kanban;

use Kanban;
use Kanban\Component;

use Silex;
use Silex\Application;

class Kernel extends Component\Kernel
{

    /**
     * @param Application $app
     */
    protected function registerControllers($app)
    {
        $app['board.controller'] = function() use ($app) {
            return new Kanban\Controller\BoardController($app);
        };

        $app['card.controller'] = function() use ($app) {
           return new Kanban\Controller\CardController($app);
        };

        $app['milestone.controller'] = function() use ($app) {
            return new Kanban\Controller\MilestoneController($app);
        };

        $app['comment.controller'] = function() use ($app) {
            return new Kanban\Controller\CommentController($app);
        };

        $app['group.controller'] = function() use ($app) {
            return new Kanban\Controller\GroupController($app);
        };

        $app['label.controller'] = function() use ($app) {
            return new Kanban\Controller\LabelController($app);
        };

        $app['security.controller'] = function() use ($app) {
            return new Kanban\Controller\SecurityController($app);
        };

        $app['user.controller'] = function() use ($app) {
            return new Kanban\Controller\UserController($app);
        };
    }

    /**
     * @param Application $app
     */
    protected function registerServices($app)
    {
        $app->register(new Silex\Provider\GitlabApiSilexProvider());
        $app->register(new Silex\Provider\SecurityJWTServiceProvider());
        $app->register(new Silex\Provider\SecurityServiceProvider());
        $app->register(new Silex\Provider\ServiceControllerServiceProvider());
        $app->register(new Component\Provider\AmqpServiceProvider());
        $app->register(new Component\Provider\RouteServiceProvider());
        $app->register(new Component\Provider\RedisServiceProvider());
    }
}
