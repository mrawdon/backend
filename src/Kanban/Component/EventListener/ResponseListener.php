<?php

namespace Kanban\Component\EventListener;


use Doctrine\DBAL\Portability\Connection;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\ResourceAbstract;
use Silex\Application;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Route;

class ResponseListener implements EventSubscriberInterface
{

    /**
     * @var Application
     */
    private $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $response = $event->getResponse();

        if (! $this->isValidResponse($response)) {
            return;
        }

        $route = $this->app['routes']->get(
            $event->getRequest()->get("_route"));

        if (! $this->isValidRoute($route)) {
            return;
        }

        $responseType = $route->getOption('response.type');

        if (is_array($responseType)) {
            foreach ($responseType as $type) {
                $this->{'publishTo' . ucfirst(strtolower($type))}($event, $route);
            }
        } else {
            $this->{'publishTo' . ucfirst(strtolower($responseType))}($event, $route);
        }
    }

    /**
     * @param mixed $route
     *
     * @return bool
     */
    protected function isValidRoute($route)
    {
        return $route instanceof Route && $route->hasOption('response.type');
    }

    /**
     * @param Response $response
     * @return bool
     */
    protected function isValidResponse($response)
    {
        return in_array($response->getStatusCode(),[
                Response::HTTP_OK,
                Response::HTTP_CREATED,
        ]);
    }


    /**
     * @param FilterResponseEvent $event
     * @param Route $route
     */
    protected function publishToWs(FilterResponseEvent $event, Route $route)
    {
        fastcgi_finish_request();
        $response = $event->getResponse();
        $request = $event->getRequest();
        $routeName = $request->get("_route");

        $fractal = new Manager();

        $transformer = $route->getOption('transformer');

        if (null !== $transformer) {
            if (null !== $route->getOption('includes')) {
                $fractal->parseIncludes($route->getOption('includes'));
            }
            $data = $fractal->createData(
                $this->transform($transformer, $response->getContent())
                    ->setMetaValue('event', $routeName))->toJson();
        } else {
            $data = $response->getContent();
        }
        $this->app['core.mq']->cast(
            'default',
            'board.'.$request->request->get('project_id'),
            $data
        );
    }

    /**
     * @param FilterResponseEvent $event
     * @param Route $route
     */
    protected function publishToJson(FilterResponseEvent $event, Route $route)
    {
        $response = $event->getResponse();
        $transformer = $route->getOption('transformer');

        $fractal = new Manager();

        if (null !== $transformer) {
            $response->setContent(
                $fractal->createData($this->transform($transformer, $response->getContent()))
                    ->toJson()
            );
        }
    }


    /**
     * @param array  $transformer
     * @param mixed  $data
     *
     * @return ResourceAbstract
     */
    protected function transform($transformer, $data)
    {
        $resource = 'League\\Fractal\\Resource\\' . ucfirst($transformer['type']);
        $class = 'Kanban\\Transformer\\' . $transformer['class'];

        return new $resource($this->unserialize($data), new $class($this->app));
    }

    /**
     * @param $data
     * @return array|object
     */
    protected function unserialize($data)
    {
        if (is_string($data)) {
            $data = unserialize($data);
        }

        return $data;
    }


    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::RESPONSE => 'onKernelResponse',
        );
    }
}