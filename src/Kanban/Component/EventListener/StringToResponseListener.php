<?php

namespace Kanban\Component\EventListener;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * hack чтобы обмануть респонс без видимости в контроллере
 *
 * Class StringToResponseListener
 *
 * @package Kanban\Services
 */
class StringToResponseListener implements EventSubscriberInterface
{

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $response = $event->getControllerResult();

        if (is_object($response) && ! $response instanceof Response || is_array($response)) {
            $event->setResponse(new Response(serialize($response)));
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::VIEW => array('onKernelView'),
        );
    }
}