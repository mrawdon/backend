<?php

namespace Kanban\Component\EventListener;

use Guzzle\Http\Exception\BadResponseException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionHandler implements EventSubscriberInterface
{

    public function __construct($app){

    }

    public function onSilexError(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof BadResponseException && false !== strpos($exception->getMessage(), '[status code] 401')) {
            $event->setResponse(new Response('Unauthorized', Response::HTTP_UNAUTHORIZED));
            return;
        }

        if ($exception instanceof BadResponseException && false !== strpos($exception->getMessage(), '[status code] 403')) {
            $event->setResponse(new Response('Forbidden', Response::HTTP_FORBIDDEN));
            return;
        }        
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(KernelEvents::EXCEPTION => 'onSilexError');
    }
}