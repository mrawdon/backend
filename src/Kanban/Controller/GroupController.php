<?php

namespace Kanban\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GroupController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function indexAction(Request $request)
    {
        $per_page = $request->query->get("per_page", 1000);

        $response = $this->app['gitlab_api']->executeCommand('GetGroups', ['per_page' => $per_page]);

        return $response;
    }
}