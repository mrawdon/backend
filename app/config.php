<?php

$gitlabHost              = getenv('GITLAB_HOST');
$gitlabBasicLogin        = getenv('GITLAB_BASIC_LOGIN');
$gitlabBasicPassword     = getenv('GITLAB_BASIC_PASSWORD');
$gitlabOauthClientId     = getenv('GITLAB_OAUTH_CLIENT_ID');
$gitlabOauthClientSecret = getenv('GITLAB_OAUTH_CLIENT_SECRET');
$secretKey               = getenv('KANBAN_SECRET_KEY');
$currentHost             = $_SERVER['HTTP_HOST'];
$currentScheme           = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http';

return array(
    'redis.options' => [
        'host'     => 'redis',
        'port'     => 6379,
        'timeout'  => 0,
        'dbname'   => 'kanban',
        'password' => 'qwerty'
    ],
    'gitlab_api.options' => [
        'base_url' => $gitlabHost,
        'base_path' => 'api/v3/',
        'request_options' => [
            'auth'          => [$gitlabBasicLogin, $gitlabBasicPassword],
            'private_token' => 'qwerty',
        ],
        'oauth2' => [
            "client_id" => $gitlabOauthClientId,
            "client_secret" => $gitlabOauthClientSecret,
            "redirect_uri"  => $currentScheme.'://'.$currentHost.'/assets/html/user/views/oauth.html'
        ]
    ],
    'security.jwt' => [
        'secret_key' => $secretKey,
        'life_time'  => 86400,
        'options' => [
            'header_name' => 'X-KB-Access-Token'
        ]
    ],
    'amqp.config' => require_once 'amqp.php',
    'router.options' => [
        'prefix' => '',
        'routes' => require_once 'routes.php',
    ]
);
