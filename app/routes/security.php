<?php

return array(
    'security.login'     => [
        'path'     => '/login',
        'defaults' => [
            '_controller' => 'security.controller:loginAction'
        ],
        'methods'  => [
            'POST'
        ]
    ],

    'security.register'     => [
        'path'     => '/register',
        'defaults' => [
            '_controller' => 'security.controller:registerAction'
        ],
        'methods'  => [
            'POST'
        ]
    ],

    'security.save.token'     => [
        'path'     => '/save_token',
        'defaults' => [
            '_controller' => 'security.controller:saveTokenAction'
        ],
        'methods'  => [
            'POST'
        ]
    ],

    'security.oauth.redirect' => [
        'path'     => '/oauth',
        'defaults' => [
            '_controller' => 'security.controller:oauthRedirectAction'
        ],
        'methods'  => [
            'GET'
        ]
    ],

    'security.oauth.authorise' => [
        'path'     => '/oauth',
        'defaults' => [
            '_controller' => 'security.controller:oauthAction'
        ],
        'methods'  => [
            'POST'
        ]
    ],

    'security.oauth.credential' => [
        'path'     => '/credential',
        'defaults' => [
            '_controller' => 'security.controller:addCredentialAction'
        ],
        'methods'  => [
            'POST'
        ]
    ]
);