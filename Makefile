IMAGE=leanlabs/backend
TAG=1.1.8

help:
	@echo "Here will be brief doc"

test:
	#todo create tests

build:
	@docker run --rm -v $(CURDIR):/data -v $$HOME/.composer/cache:/cache imega/composer:1.1.0 update

release:
	@docker run --rm -v $(CURDIR):/data -v $$HOME/.composer/cache:/cache imega/composer:1.1.0 update --no-dev
	@docker build -t $(IMAGE) .
	@docker tag $(IMAGE):latest $(IMAGE):$(TAG)
	@docker push $(IMAGE):latest
	@docker push $(IMAGE):$(TAG)


doc.html: spec
	@docker run --rm -v $(CURDIR):/data leanlabs/raml-doc-builder generate --input=spec/api.raml --output=doc.html

.PHONY: help test build release
